> Auteur : Jordan Protin | jordan.protin@yahoo.com

# React Native - Movie Carousel

Il s'agit d'un carousel animé (ou slider animé) afin de présenter des films.

[DEMO](https://gitlab.com/jordanprotin/public/react-native/react-native-movie-carousel/-/raw/master/docs/assets/images/carousel.gif)

> Réalisé à l'aide de [React Native Animated API](https://reactnative.dev/docs/animated), [FlatList](https://reactnative.dev/docs/flatlist) et [react-native-linear-gradient](https://github.com/react-native-linear-gradient/react-native-linear-gradient)

## Installation

```bash
$ git clone git@gitlab.com:jordanprotin/public/react-native/react-native-movie-carousel.git
$ cd react-native-movie-carousel
$ npm install
```

## Lancement

```bash
$ cd react-native-movie-carousel
$ npx react-native start
$ npx react-native run-android
```
