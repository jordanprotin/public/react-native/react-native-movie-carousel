import React from 'react';

import { Text, View } from 'react-native';
import styles from './Styles';

// =================================================================================

const Loading = () => (
  <View style={styles.loadingContainer}>
    <Text style={styles.loadingText}>Chargement...</Text>
  </View>
);

export default Loading;