import React from 'react';

import {
  Animated,
  Dimensions,
  FlatList,
  Image,
  View
} from 'react-native';
import { BACKDROP_HEIGHT, ITEM_SIZE } from '../../Constants';
import LinearGradient from 'react-native-linear-gradient';
import styles from './Styles';

// =================================================================================

const { width } = Dimensions.get('window');

// =================================================================================

const CarouselBackdrop = ({ stories, scrollX }) => {
  // ----
  const _renderItem = ({ item, index }) => {
    if (!item.backdrop) {
      return null;
    }

    const translateX = scrollX.interpolate({
      inputRange: [(index - 2) * ITEM_SIZE, (index - 1) * ITEM_SIZE],
      outputRange: [0, width]
    });
    
    const opacity = scrollX.interpolate({
      inputRange: [
        (index - 2) * ITEM_SIZE, 
        (index - 1) * ITEM_SIZE, 
        index * ITEM_SIZE
      ],
      outputRange: [0, 0.9, 0],
      extrapolate: 'clamp'
    });

    return (
      <Animated.View
        removeClippedSubviews={false}
        style={[ styles.item, { width: translateX, opacity }]}
      >
        <Image 
          source={item.backdrop}
          style={styles.itemImage}
        />
      </Animated.View>
    );
  };

  // ----
  return (
    <View style={{ height: BACKDROP_HEIGHT, width, position: 'absolute' }}>
      <FlatList
        horizontal
        inverted
        data={stories.reverse()}
        keyExtractor={(item) => `${item.key}-backdrop`}
        removeClippedSubviews={false}
        contentContainerStyle={{ width, height: BACKDROP_HEIGHT }}
        renderItem={_renderItem}
      />

      <LinearGradient
        colors={['rgba(0, 0, 0, 0)', '#18191C']}
        style={styles.linearGradient}
      />
    </View>
  );
};

export default CarouselBackdrop;