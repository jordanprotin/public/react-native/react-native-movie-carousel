import { Dimensions, StyleSheet } from 'react-native';
import { BACKDROP_HEIGHT } from '../../../Constants';

// =================================================================================

const { width, height } = Dimensions.get('window');

// =================================================================================

export default StyleSheet.create({
  item: {
    position: 'absolute',
    height,
    overflow: 'hidden',
  },
  itemImage: {
    width,
    height: BACKDROP_HEIGHT,
    position: 'absolute'
  },
  linearGradient: {
    height: BACKDROP_HEIGHT,
    width,
    position: 'absolute',
    bottom: 0,
  }
});