export default {
  alita: {
    poster: require('../Assets/Images/alita-poster.jpeg'),
    backdrop: require('../Assets/Images/alita-poster.jpeg')
  },
  avengers: {
    poster: require('../Assets/Images/avengers-poster.jpeg'),
    backdrop: require('../Assets/Images/avengers-backdrop.jpeg')
  },
  johnWick: {
    poster: require('../Assets/Images/john-wick-poster.jpg'),
    backdrop: require('../Assets/Images/john-wick-backdrop.jpg')
  }
};