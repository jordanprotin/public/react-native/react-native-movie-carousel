import React from 'react';
import Root from './Containers/App';

// USING CLASS COMPONENT AS A WORKAROUND FOR HOT RELOADING
export default class App extends React.Component {
  render() {
    return <Root />;
  }
}
