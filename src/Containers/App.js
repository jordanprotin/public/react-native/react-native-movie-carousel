/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, { 
  useEffect, 
  useRef, 
  useState 
} from 'react';

import {
  Animated,
  Image,
  Platform,
  Pressable,
  StatusBar,
  Text,
  View
} from 'react-native';
import { EMPTY_ITEM_SIZE, ITEM_SIZE } from '../Constants';
import { CarouselBackdrop, Loading } from '../Components';
import Images from '../Themes/Images';

import styles from './Styles';

// =================================================================================

const INITIAL_STATE = {
  stories: []
};

// =================================================================================

export default function App() {
  const AnimatedPressable = Animated.createAnimatedComponent(Pressable);

  const scrollX = useRef(new Animated.Value(0)).current;
  const [stories, setStories] = useState(INITIAL_STATE.stories);

  // ----
  useEffect(() => {
    const fetchData = () => {
      const items = [
        {
          key: 1,
          title: 'Alita: Battle Angel',
          poster: Images.alita.poster,
          backdrop: Images.alita.backdrop
        },
        {
          key: 2,
          title: 'John Wick Chapter 3',
          poster: Images.johnWick.poster,
          backdrop: Images.johnWick.backdrop
        },
        {
          key: 3,
          title: 'Avengers: End Game',
          poster: Images.avengers.poster,
          backdrop: Images.avengers.backdrop,
        }
      ];

      // add empty item to create fake space
      setStories([
        { key: 'empty-left' }, 
        ...items, 
        { key: 'empty-right' }
      ]);
    };

    if (stories.length === 0) {
      fetchData();
    }
  }, [stories]);

  // ----
  const _renderItem = ({ item, index }) => {
    if (!item.poster) {
      return <View style={{ width: EMPTY_ITEM_SIZE }} />;
    }

    const inputRange = [
      (index - 2) * ITEM_SIZE,
      (index - 1) * ITEM_SIZE,
      index * ITEM_SIZE,
    ];

    const translateY = scrollX.interpolate({
      inputRange,
      outputRange: [100, 50, 100],
      extrapolate: 'clamp',
    });

    const opacity = scrollX.interpolate({
      inputRange,
      outputRange: [0.2, 0.9, 0.2],
      extrapolate: 'clamp'
    });

    return (
      <View style={{ width: ITEM_SIZE }}>
        <AnimatedPressable
          onPress={() => console.log('onPress!')}
          style={[styles.item, {
            opacity,
            transform: [{ translateY }]
          }]}
        >
          <Image
            source={item.poster}
            style={styles.itemImage}
          />

          <Text style={styles.itemTitle} numberOfLines={1}>
            {item.title}
          </Text>
        </AnimatedPressable>
      </View>
    );
  };

  // ----
  if (stories.length === 0) {
    return <Loading />;
  }

  // ----
  return (
    <View style={styles.container}>

      <CarouselBackdrop stories={stories} scrollX={scrollX} />

      <StatusBar hidden />

      <Animated.FlatList
        showsHorizontalScrollIndicator={false}
        data={stories}
        keyExtractor={(item) => `${item.key}`}
        horizontal
        bounces={false}
        decelerationRate={Platform.OS === 'ios' ? 0 : 0.98}
        renderToHardwareTextureAndroid
        contentContainerStyle={{ alignItems: 'center' }}
        snapToInterval={ITEM_SIZE}
        snapToAlignment='start'
        snapToAlignment='start'
        onScroll={Animated.event(
          [{ nativeEvent: { contentOffset: { x: scrollX } } }],
          { useNativeDriver: false }
        )}
        scrollEventThrottle={16}
        renderItem={_renderItem}
      />
    </View>
  );
}