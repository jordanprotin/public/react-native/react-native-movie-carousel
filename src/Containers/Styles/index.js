import { StyleSheet } from 'react-native';
import { ITEM_SIZE } from '../../Constants';

// =================================================================================

const SPACING = 10;

// =================================================================================

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#18191C'
  },
  item: {
    marginHorizontal: SPACING,
    padding: SPACING * 2,
    alignItems: 'center'
  },
  itemImage: {
    width: '100%',
    height: ITEM_SIZE * 1.2,
    resizeMode: 'cover',
    borderRadius: 24,
    margin: 0,
    marginBottom: 10,
  },
  itemTitle: {
    fontSize: 24,
    color: '#807E8F'
  }
});